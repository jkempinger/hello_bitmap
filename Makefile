all:	c cpp python

c:
	tail -n+2 helloworld_c.bmp | gcc -o helloworld_c -x c -
	./helloworld_c
cpp:
	tail -n+3 helloworld_cpp.bmp | g++ -o helloworld_cpp -x c++ -
	./helloworld_cpp

python:
	tail -n+2 helloworld_py.bmp | python3 -
	
clean:
	rm -f helloworld_c helloworld_cpp
